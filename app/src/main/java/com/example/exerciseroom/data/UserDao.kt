package com.example.exerciseroom.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface UserDao {
    @Query("SELECT * FROM User")
    fun getListUser(): Single<MutableList<User>>

    @Query("SELECT * FROM User WHERE uid = :id")
    fun getUserById(id: Int): Flowable<User>

    //    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @Insert
    fun insertUser(user: User): Completable

    @Query("DELETE FROM User")
    fun deleteAllUsers()
}
