package com.example.exerciseroom.ui

import androidx.lifecycle.ViewModel
import com.example.exerciseroom.data.User
import com.example.exerciseroom.data.UserDao
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

class UserViewModel(private val dataSource: UserDao) : ViewModel() {

    fun userName(): Single<MutableList<User>> {
        return dataSource.getListUser()
    }

    fun updateUserName(firstName: String,lastName:String): Completable {
        val user = User(0,firstName,lastName)
        return dataSource.insertUser(user)
    }

    companion object {
        const val USER_ID =1
    }
}
