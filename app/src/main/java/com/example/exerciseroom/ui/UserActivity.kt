package com.example.exerciseroom.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.DocumentsContract
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.lifecycle.ViewModelProvider
import com.example.exerciseroom.R
import com.example.exerciseroom.data.UsersDatabase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_user.*

const val CREATE_FILE = 1
const val PICK_PDF_FILE = 2

class UserActivity : AppCompatActivity() {
    @SuppressLint("CheckResult")
    private lateinit var viewModelFactory: ViewModelFactory
    private val viewModel: UserViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(UserViewModel::class.java)
    }
    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        val dataSource = UsersDatabase.getInstance(this).userDao()
        viewModelFactory = ViewModelFactory(dataSource)
    }

    override fun onResume() {
        super.onResume()
        buttonAddUser.setOnClickListener {
            updateUserName()
//            createFile("my.txt".toUri())
        }
        buttonGetUser.setOnClickListener {
            getListUser()
//            openFile("my.txt".toUri())
        }
    }

    private fun updateUserName() {
        val firstName = textViewFirstName.text.toString()
        val lastName = textViewLastName.text.toString()
        disposable.add(
            viewModel.updateUserName(firstName, lastName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ },
                    { error -> })
        )
    }

    private fun getListUser() {
        disposable.add(
            viewModel.userName()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ textViewListUser.text = it.toString() },
                    { error -> })
        )
    }

    private fun createFile(pickerInitialUri: Uri) {
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "application/pdf"
            putExtra(Intent.EXTRA_TITLE, "invoice.pdf")


            // Optionally, specify a URI for the directory that should be opened in
            // the system file picker before your app creates the document.
            putExtra(DocumentsContract.EXTRA_INITIAL_URI, pickerInitialUri)
        }
        startActivityForResult(intent, CREATE_FILE)
    }

    fun openFile(pickerInitialUri: Uri) {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "application/pdf"

            // Optionally, specify a URI for the file that should appear in the
            // system file picker when it loads.
            putExtra(DocumentsContract.EXTRA_INITIAL_URI, pickerInitialUri)
        }

        startActivityForResult(intent, PICK_PDF_FILE)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }
}
